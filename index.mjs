

import LogUtil from 'zunzun/flyutil/log.mjs'

import Assert from './assert.mjs'
import Sequence from './sequence.mjs'
import Test from './test.mjs'
import Skip from './skip.mjs'

export default class FlyTest {
  static Assert = Assert
  static Sequence = Sequence
  static Test = Test
  static Skip = Skip
}
