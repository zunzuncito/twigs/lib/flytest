
import fs from 'fs'
import net from 'net'
import os from 'os'
import path from 'path'

import FlyModule from "zunzun/flymodule/index.mjs"
import TwigsUtil from 'zunzun/flyutil/twigs.mjs'

export default class HookManager {
  constructor(cwd, cfg, sbj_cfg, env_cfg, sm) {
    this.cfg = cfg;

    this.hooks = {};
    this.started_hooks = {};

    const layers = TwigsUtil.get_layers(sbj_cfg, env_cfg);
    const twigs_dir = path.join(cwd, "twigs");
    for (let i = layers.length-1; i >= 0; i--) {
      const layer = layers[i];
      for (let twig of layer) {
        const twigs_type_dir = path.join(twigs_dir, twig.type);
        const tests_dir = path.join(twigs_type_dir, twig.name, "test-flight");
        const tools_path = path.join(tests_dir, "hooks");
        const tools_index_path = path.join(tools_path, "index.mjs");
        if (fs.existsSync(tools_index_path)) {
          this.hooks[`${twig.type}/${twig.name}`] = tools_path;
        }
      }
    }

    const subject_tests_dir = path.join(cwd, 'test-flight');
    const subject_hooks_path = path.join(subject_tests_dir, "hooks");
    const subject_hooks_index_path = path.join(subject_hooks_path, "index.mjs");
    if (fs.existsSync(subject_hooks_index_path)) {
      this.hooks[`subject`] = subject_hooks_path;
    }


  

    const _this = this;

    this.tcp_client = net.createConnection(cfg.tcp_port, cfg.tcp_addr, () => {
      console.log('  FlyTest > Established connection with testing client (CLI)!');

      if (cfg.subject_name) {
        _this.tcp_client.write(JSON.stringify({
          subject_name: cfg.subject_name
        })+"\n");
      }

      this.tcp_client.on('data', async (data) => {


        const msgs = data.toString().split('\n').filter((msg) => msg.trim().length > 0).map((msg) => JSON.parse(msg));;
        for (let msg of msgs) {
          if (msg.cmd) {
            switch (msg.cmd) {
              case 'start':
                console.log(`  FlyTest > Starting test hook: ${_this.hooks[msg.target]}@${msg.class}`);
                const test_module = await FlyModule.load(_this.hooks[msg.target], "index.mjs");
                _this.started_hooks[`${msg.target}@${msg.class}`] = await test_module.default[msg.class].construct(sm);
                _this.tcp_client.write(JSON.stringify({
                  info: "hook-started",
                  target: msg.target,
                  class: msg.class
                })+"\n");
                break;
              case 'call':
                console.log(`  FlyTest > Call test hook method: ${_this.hooks[msg.target]}@${msg.class} ${msg.method}(${msg.args})`);
                const method_args = Array.isArray(msg.args) ? msg.args : [msg.args];
                const returned_value = await _this.started_hooks[`${msg.target}@${msg.class}`][msg.method](...method_args);
                _this.tcp_client.write(JSON.stringify({
                  info: "method-called",
                  target: msg.target,
                  class: msg.class,
                  return: returned_value
                })+"\n");
                break;
              case 'stop':
                await _this.started_hooks[`${msg.target}@${msg.class}`].destroy();
                delete _this.started_hooks[`${msg.target}@${msg.class}`];
                break;
              default:
            }
          } else if (msg.done_task) {
            _this.browser_socket.send(JSON.stringify(msg));
          } else if (msg.browser) {
            if (msg.browser) {
              _this.browser_socket.send(JSON.stringify(msg.browser));
            }
          }
        }
      });

    });

    this.tcp_client.on('error', (err) => {
      console.log(err);
    });


    this.tcp_client.on('end', () => {
      console.log('Disconnected from server');
    });

  }

  set_browser_socket(sock) {
    this.browser_socket = sock;
  }
}
