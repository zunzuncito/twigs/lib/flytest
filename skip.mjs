
import LogUtil from 'zunzun/flyutil/log.mjs'

export default class Skip {
  constructor(msg) {
    LogUtil.msg([1, 4, 40, 90, ` SKIPPING: ${msg} `, 0]);
    LogUtil.nl();
  }
}
