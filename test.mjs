


import TimeUtil from 'zunzun/flyutil/time.mjs'
import LogUtil from 'zunzun/flyutil/log.mjs'
import Skip from './skip.mjs'

export default class Test {
  static test(msg, test_func) {
    LogUtil.nl();
    LogUtil.msg([1, 4, 40, 33, " TESTING >>> ", msg, " ", 0]);
    try {
      test_func();
      LogUtil.msg([1, 4, 40, 32, " SUCCESS ", 0]);
      LogUtil.nl();
    } catch (e) {
      if (
        !(e instanceof Skip)
      ) {
        LogUtil.msg([1, 4, 37, 41, ` TEST FUNCTION "${msg}" FAILURE `, 0]);
        console.error(e.stack);
        LogUtil.nl();
        throw e;
      }
    }
  }

  static async test_async(msg, test_func) {
    const start_time = Date.now();
    LogUtil.nl();
    LogUtil.msg([1, 4, 40, 33, " TESTING >>> ", msg, " ", 0]);
    try {
      await test_func();
      const test_took = TimeUtil.format(
        Date.now() - start_time,
        ['hh', ':', 'mm', ':', 'ss', ':', 'ms']
      );
      LogUtil.msg([1, 4, 40, 32, " SUCCESS ", 0], [1, 40, 36, `| TOOK: ${test_took}`, 0]);
      LogUtil.nl();
    } catch (e) {
      if (
        !(e instanceof Skip)
      ) {
        LogUtil.msg([1, 4, 37, 41, ` TEST FUNCTION "${msg}" FAILURE `, 0]);
        console.error(e.stack);
        LogUtil.nl();
        throw e;
      }
    }

  }

  static fail (...msgs) {
    LogUtil.nl();
    LogUtil.msg([1, 31, " ERROR: ", 0]);
    for (let msg of msgs) {
      LogUtil.msg([1, msg, 0]);
    }
    LogUtil.nl();
    return false;
  }
}
