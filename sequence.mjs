

import LogUtil from 'zunzun/flyutil/log.mjs'
import Test from "./test.mjs"

export default class TestSequence {
  constructor() {
    this.errors = [];
  }

  test(msg, test_func) {
    try {
      Test.test(msg, test_func);
    } catch (e) {
      this.errors.push(e);
    }
  }

  async test_async(msg, test_func) {
    try {
      await Test.test_async(msg, test_func);
    } catch (e) {
      this.errors.push(e);
    }
  }

  throw() { 
    if (this.errors.length > 0) throw this.errors[0];
  }
}
