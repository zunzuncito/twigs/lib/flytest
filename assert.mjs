
import LogUtil from 'zunzun/flyutil/log.mjs'

export default class FlyTestAssert {

  static equal(a, b, msg) {
    if (!(a == b)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error(`Assertion failure: values '${a}' and '${b}' are not equal`);
    }
  }

  static not_equal(a, b, msg) {
    if (!(a !== b)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error(`Assertion failure: values equal`);
    }
  }


  static gt(a, b, msg) {
    if (!(a > b)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: first value not greater");
    }
  }

  static lt(a, b, msg) {
    if (!(a > b)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: first value not lesser");
    }
  }


  static true(val, msg) {
    if (!(typeof val == "boolean" && val)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: not true");
    }
  }

  static false(val, msg) {
    if (!(typeof val == "boolean" && !val)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: not false");
    }
  }


  static number(val, msg) {
    if (!(typeof val == "number")) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: not number");
    }
  }

  static string(val, msg) {
    if (!(typeof val == "string" && val.length > 0)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error("Assertion failure: not string");
    }
  }

  static object(val) {
    if (!(!Array.isArray(val) && typeof val == "object")) {
      throw new Error("Assertion failure: not object");
    }
  }

  static array(val, msg) {
    if (!Array.isArray(val)) {
      LogUtil.msg([37, 41, `FAIL: ${msg}`, 0]);
      throw new Error(`Assertion failure: Expected array, got ${typeof val}`);
    }
  }

  static throws(exception, func) {
    try {
      func();
    } catch (error) {
      if (error instanceof exception) {
        return;
      } else {
        throw new Error(`Assertion failure: Expected ${exception.name} to be thrown, but ${error.name} was thrown instead.`);
      }
    }

    throw new Error(`Expected ${exception.name} to be thrown, but no exception was thrown.`);
  }

}
